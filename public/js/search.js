
//search product
mainApp.controller("searchproductController",
[
    "$scope", "$http", '$timeout', function ($scope, $http, $timeout) {

        $scope.listSearchWeb = [
           {
               'pagesource': 1,
               'linkwebsite': 'https://world.taobao.com',
               'name': 'TAOBAO.COM',
           },
            {
                'pagesource': 2,
                'linkwebsite': 'https://s.1688.com',
                'name': '1688.COM',
            },
            {
                'pagesource': 3,
                'linkwebsite': 'https://list.tmall.com',
                'name': 'TMAIL.COM',
            }
        ];
        $scope.webSelected = $scope.listSearchWeb[0];

        $scope.init = function () {
           
        };

        $scope.select = function (web) {
            $scope.webSelected = web;
        };

        //click search
        $scope.searchproduct = function () {

            var website = {};
            if ($scope.webSelected.pagesource === 1) {
                website = $scope.listSearchWeb[0];
            } else if ($scope.webSelected.pagesource === 2) {
                website = $scope.listSearchWeb[1];
            } else if ($scope.webSelected.pagesource === 3) {
                website = $scope.listSearchWeb[2];
            }

            if ($scope.strproduct !== null && $scope.strproduct !== '') {
                var url = "https://www.googleapis.com/language/translate/v2?key=" + googleTranslateApi + "&source=vi&target=zh-CN&q=" + $scope.strproduct;
                $http.get(url).
                    success(function (data, status, headers, config) {
                        if ($scope.webSelected.pagesource == 1) {
                            var landingUrl = website.linkwebsite + "/search/search.htm?_ksTS=1467387141329_20&spm=a21bp.7806943.20151106.1&json=on&suggest_query=vay&cna=NK%2F2D%208%20TAkCARtM6NXRTyvU&wq=vay&suggest=0_1&_input_charset=utf-8&source=suggest&navigator=all&q=" + data.data.translations[0].translatedText + "&callback=__jsonp_cb&abtest=_AB-LR517-LR854-LR895-PR517-PR854-PV895_2461";
                            window.location.href = landingUrl;
                        } else if ($scope.webSelected.pagesource == 2) {
                            var landingUrl = website.linkwebsite + "/selloffer/offer_search.htm?_input_charset=utf-8&button_click=top&earseDirect=false&n=y&keywords=" + data.data.translations[0].translatedText;
                            window.location.href = landingUrl;
                        }
                        else if ($scope.webSelected.pagesource == 3) {
                            var landingUrl = website.linkwebsite + "/search_product.htm?q=" + data.data.translations[0].translatedText;
                            window.location.href = landingUrl;
                        }
                    }).
                    error(function (error, status, headers, config) {
                        console.log('error = ', error);
                    });
                
            } else {
                alert("Bạn phải nhập từ khóa tìm kiếm!");
            }

        };

    }


]);

//get product info by link
mainApp.controller("quickCreateCart", ["$scope", "$http", function ($scope, $http) {
    $scope.link = "";

    $scope.getProductInfo = function () {
        window.location.href = "/tao-don-hang-nhanh?url=" + $scope.link;
    }

}]);