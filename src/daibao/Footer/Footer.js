import React from 'react'
import hosting from '../../host/hosting'
function Footer() {

  const [TaiKhoan, SetTaiKhoan] = React.useState("");
  const [MatKhau, SetMatKhau] = React.useState("");
  const [KiemTraDangNhap, SetKiemTraDangNhap] = React.useState(false);
  const [DLDangNhapLucLac, SetDLDangNhapLucLac] = React.useState([]);
  const onClickDangNhap = async () => {
    const reponse = await fetch(hosting.DangNhapLucLac, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ TaiKhoan, MatKhau }),
    });
    const JsonData = await reponse.json();
    alert(JsonData.message);
    if (JsonData.status === 1) {
      window.localStorage.setItem("DangNhapLucLac", "true");
      window.localStorage.setItem(
        "DLDangNhapLucLac",
        JSON.stringify(JsonData.data)
      );
      console.log(TaiKhoan + " " + MatKhau);
      window.location.href = "./dangnhapkhachhang";
    } else {
      alert('Đăng nhập thất bại!')
      window.localStorage.setItem("DangNhapLucLac", "false");
    }
  };

  React.useEffect(() => {
    SetKiemTraDangNhap(
      window.localStorage.getItem("DangNhapLucLac") === "true"
    );
    SetDLDangNhapLucLac(
      JSON.parse(window.localStorage.getItem("DLDangNhapLucLac"))
    );
  }, []);
  console.log(DLDangNhapLucLac);

    return (
        <div>
            <footer className="row">
    <div className="row">
      <div className="container">
        <div className="col-xs-4 col-sm-4">
          <div className="header-footer">
            <h3>LIÊN HỆ</h3>
          </div>
          <ul className="list-unstyled link-news">
            <li>
              <a href="#">Hải Phòng</a><br />
              <i className="fa fa-home " /> Ký Con - Hồng Bàng - Hải Phòng <br />
              <i className="fa fa-phone " /> 0865150693
            </li>
            {/* 				<li>
					<a href="#">Hồ Chí Minh</a><br>
					<i class="fa fa-home "></i> Đồng Xoài, Phường 13,  Quận Tân Bình, HCM <br>
					<i class="fa fa-phone "></i> 098.158.2299 &nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp; 090.158.2299
				</li>
				<li>
					<a href="#">Trung Quốc</a><br>
					<i class="fa fa-home "></i> 广西凭祥市南大路（广越酒店对面100米）<br>
					<i class="fa fa-phone "></i> 17607888176
				</li>*/}
            <li>
              <a href="#">ĐẶT HÀNG VÀ GIẢI QUYẾT CÁC VẤN ĐỀ LIÊN QUAN ĐẾN ĐƠN HÀNG</a><br />
              <i className="fa fa-phone " />   0901568693 (24/7)
              {/* <i class="fa fa-phone "></i>   0779232624 (24/7) */}
            </li> 
            <li>
              <a href="#">SỐ SALE (24/7)</a><br />
              <i className="fa fa-phone " />  0865150693 (24/7)
            </li> 
          </ul>
        </div>
        <div className="col-xs-4 col-sm-4">
          <div className="header-footer">
            <h3>THÔNG TIN CHUYỂN KHOẢN</h3>
          </div>
          <div className="clearfix">
            <div className="pull-left" style={{width: '49%'}}><ul className="list-unstyled link-news">
                {/* 					<li>
						<a href="#">Techcombank</a><br>
						TK:   Phạm Thanh Tùng <br>
						STK: 19031260440555 <br>
					</li> */}
                <li>
                  <a href="#">Vietcombank</a><br />
                  TK:   Phạm Hồng Việt Anh <br />
                  STK: 1015168168 <br />
                </li>
                {/* 						<li>
							<a href="#">VPBANK </a><br>
							TK:   Phạm Thanh Tùng <br>
							STK: 146144306 <br>
						</li> */}
              </ul></div> 				
            {/* 				<div class="pull-left" style="width:49%"><ul class="list-unstyled link-news">
						<li>
							<a href="#">MSB </a><br>
							TK:   Phạm Thanh Tùng  <br>
							STK: 02101014944516 <br>
						</li>
						<li>
							<a href="#">Sacombank  </a><br>
							TK:   Phạm Thanh Tùng  <br>
							STK: 030062025065 <br>
						</li>
						<li>
							<a href="#">BIDV   </a><br>
							TK:   Phạm Thanh Tùng  <br>
							STK: 32110001032616 <br>
						</li>
					</ul>
				</div> */}
          </div>
        </div>
        <div className="col-xs-4 col-sm-4">
          <div className="header-footer">
            <h3>FANPAGE</h3>
          </div>
          <div>
            <div height={100} className="fb-page" data-href="https://www.facebook.com/dai68vn-100705601398143/" data-tabs="timeline" data-width data-height data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/dai68vn-100705601398143/" className="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/dai68vn-100705601398143/">order0phi.vn</a></blockquote></div>
          </div>
        </div>
      </div>
    </div>
    
  </footer>

        </div>
    )
}

export default Footer
