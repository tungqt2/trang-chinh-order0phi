import React, { Fragment } from 'react'

import LoGoOrder from '../assets/order0phi.jpg'
import TrangChinh from './Body/TrangChinh/TrangChinh'
import Header from './Header/Header'
import Footer from './Footer/Footer'
import Main from './Main'
function DaiBao() {
    const [Tab,SetTab] = React.useState('Index')
    const onTab = (e)=>{
      SetTab(e)
    }
    return (
      <Fragment>
        <Header onTab={onTab}></Header>
        <Main Tab={Tab}></Main>
        {/* <Footer></Footer> */}
        <div style={{height:'200px'}}></div>
      </Fragment>
    )
}

export default DaiBao
