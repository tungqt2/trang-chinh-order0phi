import React from 'react'

import LoGoOrder from '../../../assets/order0phi.jpg'
function TrangChinh() {
    return (
        <div>
 <div className="container" id="app-container">
  
  <div id="main" className="row">
    <div className="row home-banner">
      <div className="container">
        <div className="row" style={{marginBottom: '15px'}}>
          <div className="col-xs-12 col-sm-12 col-md-6">
            <div style={{fontWeight: 'bold', marginTop: '10px', textTransform: 'uppercase', textAlign: 'center', marginBottom: '5px', fontSize: '16px'}}>
              Công cụ đặt hàng trên các website: Taobao, Tmall, 1688.com
            </div>
          </div>
          <div className="col-xs-12 col-sm-12 col-md-6">
            <div style={{fontWeight: 'bold', marginTop: '10px', textTransform: 'uppercase', textAlign: 'center', marginBottom: '5px', fontSize: '16px'}}>
              Facebook và video hướng dẫn
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="nhtq-info-box">
              <a href="https://chrome.google.com/webstore/detail/order-china/gfpdenlhkbpoomgkmnllnlkdbpfmaebh" target="_blank" rel="nofollow">
                <img className="img-responsive" src="images/chrome_installer.png" alt="Cài đặt cho trình duyệt Google Chrome" />
              </a>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="nhtq-info-box">
              <a href="https://chrome.google.com/webstore/detail/order-china/gfpdenlhkbpoomgkmnllnlkdbpfmaebh" target="_blank" rel="nofollow">
                <img className="img-responsive" src="images/coccoc_installer.png" alt="Cài đặt cho trình duyệt Coccoc" />
              </a>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="nhtq-info-box video-facebook">
              <div height={100} className="fb-page" data-href="https://www.facebook.com/Order0phi-101594072150854" data-tabs="timeline" data-width data-height data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/dai68vn-100705601398143/" className="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/dai68vn-100705601398143/">order0phi.vn</a></blockquote></div>
            </div>
          </div>
          {/* <div className="col-md-3 col-sm-6 col-xs-12">
            <a target="_blank" href="https://apps.apple.com/vn/app/dai68-vn/id1490229151"><img src="images/ios.png" style={{width: '80%', marginBottom: '20px'}} /></a>
            <a target="_blank" href="https://play.google.com/store/apps/details?id=com.dai68&hl=en"><img src="images/android.png" style={{width: '80%'}} /></a>
          </div> */}
        </div>
      </div>
    </div>
    {/* <div className="container">
      <div className="box">
        <div className="row home-banner">
          <img src="images/banner.jpg" style={{display: 'block', margin: '0 auto', maxWidth: '100%'}} />
        </div>
      </div>
    </div> */}
    <div className="quytrinh">
      <div className="v2-commit v2-commit-buy bg-gray">
        <div className="container">
          <div className="box">
            <div className="box-title"><div className="box-title-name" style={{fontWeight: 750}}>Quy trình <span style={{fontWeight: 750}}>đặt mua hàng hóa</span></div></div>
            <div className="box-content">
              <div className="v2-commit-inner">
                <div className="v2-commit-step col5">
                  <div className="vcs-img">
                    <img src="images/user.png" alt="step1" />
                  </div>
                  <div className="vcs-number">1</div>
                  <a href="#">
                    Đăng ký<br /> tài khoản
                    <span>Hướng dẫn</span>
                  </a>
                </div>{/* v2-commit-step */}
                <div className="v2-commit-step col5">
                  <div className="vcs-img"><img src="images/cloud.png" alt="step2" /></div>
                  <div className="vcs-number">2</div>
                  <a href="#">
                    Cài đặt công cụ<br /> đặt hàng
                    <span>Hướng dẫn</span>
                  </a>
                </div>{/* v2-commit-step */}
                <div className="v2-commit-step col5">
                  <div className="vcs-img"><img src="images/cart.png" alt="step3" /></div>
                  <div className="vcs-number">3</div>
                  <a href="#">
                    Chọn mua và tạo đơn hàng
                    <br />tại các website Trung Quốc
                    <span>Hướng dẫn</span>
                  </a>
                </div>{/* v2-commit-step */}
                <div className="v2-commit-step col5">
                  <div className="vcs-img"><img src="images/payment.png" alt="step4" /></div>
                  <div className="vcs-number">4</div>
                  <a href="#">
                    Chuyển tiền
                    <br />đặt cọc cho chúng tôi
                    <span>Hướng dẫn</span>
                  </a>
                </div>{/* v2-commit-step */}
                <div className="v2-commit-step col5">
                  <div className="vcs-img"><img src="images/tranfer.png" alt="step5" /></div>
                  <div className="vcs-number">5</div>
                  <a href="#">
                    Thanh toán và
                    <br />nhận hàng
                    <span>Hướng dẫn</span>
                  </a>
                </div>{/* v2-commit-step */}
                <div className="clearfix" />
              </div>{/* v2-commit-inner */}
            </div>{/* box-content */}
          </div>{/* box */}
        </div>{/* container */}
      </div>
    </div>
    <div className="row descrription">
      <div className="container">
        <div className="col-xs-4">
          <div className="description-header">
            <h2>Đặt hàng Trung Quốc - Việt Nam</h2>
          </div>
          <div className="description-content">
            <h3>
              Đặt hàng trên các website Thương Mại Điện Tử lớn của Trung Quốc như : taobao.com, tmall.com, 1688.com, pinduoduo.com, alibaba.com…
            </h3>
          </div>
        </div>
        <div className="col-xs-4">
          <div className="description-header">
            <h2>Vận chuyển hàng Trung Quốc - Việt Nam</h2>
          </div>
          <div className="description-content">
            <h3>
              Dịch vụ  vận chuyển Trung Quốc - Việt Nam mang đến sự thuận tiện, tiết kiệm thời gian chi phí và sự yên tâm của khách hàng.
            </h3>
          </div>
        </div>
        {/* <div className="col-xs-3">
          <div className="description-header">
            <h2>Tư vấn và hỗ trợ nguồn hàng Trung Quốc</h2>
          </div>
          <div className="description-content">
            <h3>
              Phục vụ tư vấn chuyên nghiệp am hiểu nguồn hàng Trung Quốc. Dễ dàng thương lượng giá cả tiết kiệm chi phí tối đa cho khách hàng.
            </h3>
          </div>
        </div> */}
        <div className="col-xs-4">
          <div className="description-header">
            <h2>Chuyển đổi tiền tệ Việt Nam - Trung Quốc</h2>
          </div>
          <div className="description-content">
            <h3>
              Dịch vụ chuyển khoản nạp tiền Alipay, Taobao nhanh chóng.
            </h3>
          </div>
        </div>
      </div>
    </div>
    {/* Your customer chat code */}
    <div className="fb-customerchat" attribution="setup_tool" page_id={100705601398143} theme_color="#0084ff" logged_in_greeting="Xin chào, Dai68 có thể giúp gì cho bạn?" logged_out_greeting="Xin chào, Dai68 có thể giúp gì cho bạn?">
    </div>
  </div>

</div>

        </div>
    )
}

export default TrangChinh
