import React from 'react'
import _radomstring from "randomstring";

import hosting from '../../../host/hosting'
function DangKy() {
  const [radomstring, Setradomstring] = React.useState(
    _radomstring.generate(8)
  );
  const [capcha, Setcapcha] = React.useState("");
  const [Email, SetEmail] = React.useState("");
  const [SDT, SetSDT] = React.useState("");
  const [DiaChi, SetDiaChi] = React.useState("");
  const [MatKhau, SetMatKhau] = React.useState("");
  const [XacNhanMatKhau, SetXacNhanMatKhau] = React.useState("");
  const [TaiKhoan, SetTaiKhoan] = React.useState("");
  const [TenVaTenDem, SetTenVaTenDem] = React.useState("");
  const [LinkFb, SetLinkFb] = React.useState("");
  const [Ho, SetHo] = React.useState("");
  const onClickDangKyTaiKhoan = async () => {
    if (MatKhau.length < 6 && XacNhanMatKhau.length < 6) {
      alert("Xin vui lòng nhập mật khẩu người dùng (độ dài 6-18)!");
    } else if (MatKhau !== XacNhanMatKhau) {
      alert("Xác nhận mật khẩu không khớp!");
    } else if (Email.indexOf("@gmail.com") <= 0) {
      alert("Người dùng nhập sai email!");
    } else {
      const response = await fetch(hosting.DangKyTaiKhoanLucLac, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          TaiKhoan,
          TenVaTenDem,
          DiaChi,
          Ho,
          SDT,
          Email,
          MatKhau,
        }),
      });
      const thong_Bao = await response.json();
      alert(thong_Bao.message);
      window.localStorage.setItem("Tab", "Index");
      console.log("test");
      window.location.href = "./";
    }
  };
    return (
        <div className={window.innerWidth <= 769 ? "container" : ""}>
          
            <div id="main" className="row">
  <div className="row">
    <div className="containter">
      <div className="login" style={{marginTop: '50px'}}>
          <input type="hidden" name="_token" defaultValue="tnNzu1GADZwMNllwByaPOc4mtHK2GlcisXKl26AR" />
          <div className="row">
            <div className="col-sm-6 col-sm-offset-3">
              <fieldset>
              <div className="text-center"><legend>Thông tin tài khoản</legend> </div>
                
                <div className="form-group mt-3">
                  <label className="col-md-3 control-label" htmlFor="name">Họ tên</label>
                  <div className="col-md-9">
                    <input className="form-control mt-3" id="name" name="name" placeholder="Họ tên" type="text" defaultValue 
                    value={TenVaTenDem}
                    onChange={e=>SetTenVaTenDem(e.target.value)}
                    />
                  </div>
                </div>
                <div className="form-group mt-3">
                  <label className="col-md-3 control-label" htmlFor="phone_number">Số điện thoại</label>
                  <div className="col-md-9">
                    <input className="form-control mt-3" id="phone_number" name="phone_number" 
                      value={SDT}
                      onChange={e=>SetSDT(e.target.value)}
                    placeholder="Số điện thoại" type="text" defaultValue />
                  </div>
                </div>
                <div className="form-group mt-3">
                  <label className="col-md-3 control-label" htmlFor="phone_number">Tài khoản</label>
                  <div className="col-md-9">
                    <input className="form-control mt-3" id="phone_number" name="phone_number" 
                      value={TaiKhoan}
                      onChange={e=>SetTaiKhoan(e.target.value)}
                    placeholder="Tài khoản" type="text" defaultValue />
                  </div>
                </div>
                <div className="form-group mt-3">
                  <label className="col-md-3 control-label" htmlFor="password">Mật khẩu</label>
                  <div className="col-md-9">
                    <input className="form-control mt-3" id="password" name="password" placeholder="Mật khẩu"
                      value={MatKhau}
                      onChange={e=>SetMatKhau(e.target.value)}
                    type="password" aria-autocomplete="list" />
                  </div>
                </div>
                <div className="form-group mt-3">
                  <label className="col-md-3 control-label" htmlFor="confirm-password">Xác nhận mật khẩu</label>
                  <div className="col-md-9">
                    <input className="form-control mt-3" id="confirm-password" name="confirm-password" 
                      value={XacNhanMatKhau}
                      onChange={e=>SetXacNhanMatKhau(e.target.value)}
                    placeholder="Xác nhận mật khẩu" type="password" />
                  </div>
                </div>
                
                
                <div className="form-group mt-3">
                  <label className="col-md-3 control-label" htmlFor="email">Email</label>
                  <div className="col-md-9">
                    <input className="form-control mt-3" id="email" name="email" placeholder="Email" 
                      value={Email}
                      onChange={e=>SetEmail(e.target.value)}
                    type="text" defaultValue />
                  </div>
                </div>
                <div className="form-group mt-3">
                  <label className="col-md-3 control-label" htmlFor="address">Địa chỉ</label>
                  <div className="col-md-9">
                    <input className="form-control mt-3" id="address" name="address" 
                      value={DiaChi}
                      onChange={e=>SetDiaChi(e.target.value)}
                    placeholder="Địa chỉ" type="text" defaultValue />
                  </div>
                </div>
                <div className="form-group mt-3">
                  <label className="col-md-3 control-label" htmlFor="facebook">Link Facebook</label>
                  <div className="col-md-9">
                    <input className="form-control mt-3" id="facebook" name="facebook" 
                      value={LinkFb}
                      onChange={e=>SetLinkFb(e.target.value)}
                    placeholder="Link Facebook" type="text" defaultValue />
                  </div>
                </div>
                <div className="form-group mt-3">
                  <div className="col-md-9 col-md-offset-3">
                    <button type="submit" className="btn btn-primary"
                    onClick={onClickDangKyTaiKhoan}
                    >Đăng ký</button>
                    <br />
                    <br />
                    <p>Nếu bạn đã có tài khoản, Xin vui lòng <a href="dang-nhap.html" className="text-primary">đăng nhập</a>.</p>
                  </div>
                </div>
              </fieldset>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>

        </div>
    )
}

export default DangKy
