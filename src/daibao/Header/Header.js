import React from 'react'
import LoGoOrder from '../../assets/order0phi.jpg'
import hosting from '../../host/hosting';
function Header({onTab}) {
  const [TaiKhoan, SetTaiKhoan] = React.useState("");
  const [MatKhau, SetMatKhau] = React.useState("");
  const [KiemTraDangNhap, SetKiemTraDangNhap] = React.useState(false);
  const [DLDangNhapLucLac, SetDLDangNhapLucLac] = React.useState([]);
  const onClickDangNhap = async () => {
    const reponse = await fetch(hosting.DangNhapLucLac, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ TaiKhoan, MatKhau }),
    });
    const JsonData = await reponse.json();
    alert(JsonData.message);
    if (JsonData.status === 1) {
      window.localStorage.setItem("DangNhapLucLac", "true");
      window.localStorage.setItem(
        "DLDangNhapLucLac",
        JSON.stringify(JsonData.data)
      );
      console.log(TaiKhoan + " " + MatKhau);
      window.location.href = "./dangnhapkhachhang";
    } else {
      alert('Đăng nhập thất bại!')
      window.localStorage.setItem("DangNhapLucLac", "false");
    }
  };

  React.useEffect(() => {
    SetKiemTraDangNhap(
      window.localStorage.getItem("DangNhapLucLac") === "true"
    );
    SetDLDangNhapLucLac(
      JSON.parse(window.localStorage.getItem("DLDangNhapLucLac"))
    );
  }, []);
  console.log(DLDangNhapLucLac);
    return (
        <div>
              <header className="row">
    <div className="header-top">
      <div className="container">
        <div className="row">
          <div className="col-md-4 col-xs-4 phone-number-header">
            <div className="support-top">
              <ul className="list-unstyled" style={{fontSize: '15px', fontWeight: 'bold'}}>
                <li><span><i className="fa fa-phone" /></span> 0865150693 (24/7) </li>
                <li>
                  {/* Tỷ giá NDT-VNĐ: 3750 */}
                </li>
              </ul>
            </div>
          </div>
          <div className="col-md-8 col-xs-8 lang-contact">
            <div className="account-top">
              <input type="hidden" id="hdCurrentCustomerId" />
              <ul className="pull-right">
                <li className="account-signin" style={{float: 'left'}}>
                <div>
                <a  data-toggle="modal" data-target="#loginModal" title>
                    <i className="fa fa-sign-in" aria-hidden="true" /> Đăng
                    nhập
                  </a>
                  {/* <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Launch demo modal
                  </button> */}
                  {/* Modal */}

                  <div className="modal " id="loginModal" role="dialog" aria-labelledby="popupLogin">
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header header-popup-login">
            <button type="button" className="close" id="btn-close-popupLogin" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 className="modal-title" style={{color:'black'}}>Đăng nhập</h4>
          </div>
           <div className="modal-body">
              <input type="hidden" name="_token" defaultValue="tnNzu1GADZwMNllwByaPOc4mtHK2GlcisXKl26AR" />
              <div className="area-form-regis">
                <div className="form-group">
                  <label>Tài khoản </label>
                  <input className="form-control" 
                  onChange={e=>SetTaiKhoan(e.target.value)}
                  value={TaiKhoan}
                  data-val="" data-val-length="Số điện thoại phải có ít nhất 10 ký tự. tối đá 12 ký tự" data-val-length-max={12} data-val-length-min={10} data-val-required="Bắt buộc phải nhập số điện thoại !" id="phone_number" name="phone_number" placeholder="Tài khoản" type="text" defaultValue />
                  <span className="field-validation-valid" data-valmsg-for="phone_number" data-valmsg-replace="" />
                </div>
                <div className="form-group">
                  <label>Mật khẩu </label>
                  <input className="form-control" 
                  value={MatKhau}
                  onChange={e=>SetMatKhau(e.target.value)}
                  id="password" name="password" placeholder="Mật khẩu" type="password" />
                </div>
                <a href="forgotpassword.html" title="Quên mật khẩu" className="pull-right forgot-pass">Quên mật khẩu?</a>
                <div style={{clear: 'both'}} />
                <div className="form-group" style={{marginTop: '10px'}}>
                  <button type="submit" className="btn btn-danger btn-block"
                  onClick={onClickDangNhap}
                  >Đăng nhập</button>
                </div>
                <input name="__RequestVerificationToken" type="hidden" defaultValue="nYmGM5RJC3Z2-xE0b-aag-SXd4KfKaDHPaOHjhoUg_aaoEscUyZN1stKzK1Q5Q-ls2ECxo9k8Ga-WgSdC9XsVBxtAcs1" />
              </div>
            </div>
        </div>{/* /.modal-content */}
      </div>{/* /.modal-dialog */}
</div>

                </div>

                </li>
                <li className="account-signup" style={{float: 'left'}}>
                  <a style={{cursor:'pointer'}}
                  onClick={async ()=> {
                      onTab('Đăng ký')
                  }}>
                    <i className="fa fa-user" aria-hidden="true" />
                    Đăng ký
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="header-menu">
      <div className="container">
        <nav className="navbar">
          <div className="navbar-header">
            <a style={{cursor:'pointer'}}
                  onClick={async ()=> {
                      onTab('Index')
                  }} className="logo-img">
              <img src={LoGoOrder} style={{borderRadius: '25%'}} />
            </a>
          </div>
          <ul className="nav navbar-nav">
            <li className="active"><a style={{cursor:'pointer'}}
                  onClick={async ()=> {
                      onTab('Index')
                  }}>TRANG CHỦ</a></li>
            <li><a href="#">GIỚI THIỆU</a></li>
            <li><a href="#">BÁO GIÁ</a></li>
            <li><a href="#">HƯỚNG DẪN</a></li>
            <li><a href="#">TIN TỨC</a></li>
            <li><a href="#">THÔNG BÁO</a></li>
            <li><a href="contact.html">LIÊN HỆ</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </header>
  
        </div>
    )
}

export default Header
