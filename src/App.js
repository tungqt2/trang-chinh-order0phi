/** @format */

import logo from "./logo.svg";
import "./App.css";
import React from "react";
import Header from "./views/Header/Header";
import Main from "./views/Body/Main";
import Footer from "./views/Footer/Footer";
import ThanhMeunuMobile from "./views/Header/ThanhMeunuMobile";
import hosting from "./host/hosting";
import DaiBao from "./daibao/DaiBao";
function App() {
// Dữ liệu
    const getData = async ()=>{
      const response = await fetch(hosting.LoaiHang+`/true`)
      const JsonData = await response.json()
      
      if(window.localStorage.getItem('DanhMuc')===null){
        window.localStorage.setItem('DanhMuc',JSON.stringify(JsonData))
        window.location.href = "./"
      }else{
      }
    }
    const [data,SetData] = React.useState([])
  getData()
  const [Tab, SetTab] = React.useState(
    window.localStorage.getItem("Tab") === null
      ? "Index"
      : window.localStorage.getItem("Tab")
  );
  const onTab = async (e) => {
    window.localStorage.setItem("Tab", e);
    SetTab(e);
    // console.log(e)
    window.location.reload();
  };
  React.useEffect(async () => {
    const getData = await fetch('http://103.127.207.24:3004/DLtest')
    const JsonData = await (getData.json())
    SetData(JsonData)
    console.log(window.localStorage.getItem("Tab"));
    // getData()
  }, []);
  
  return(
    <React.Fragment>
        <DaiBao></DaiBao>
        <div className="hotline-phone-ring-wrap">
        <div className="hotline-phone-ring">
          <div className="hotline-phone-ring-circle" />
          <div className="hotline-phone-ring-circle-fill" />
          <div className="hotline-phone-ring-img-circle">
            <a href="tel:0865150693" className="pps-btn-img">
              <img src="https://nguyenhung.net/wp-content/uploads/2019/05/icon-call-nh.png" alt="Gọi điện thoại" width={50} />
            </a>
          </div>
        </div>
        <div className="hotline-bar">
          <a href="tel:0865150693">
            <span className="text-hotline">0865150693</span>
          </a>
        </div>
      </div>
    </React.Fragment>
  )
  // return (
  //   <React.Fragment>
  //     {/* {data.map(x=>(
  //       <div>{x.ma_hang + ' ' + x.ten_hang}</div>
  //     ))} */}
  //     {/* Top Header */}
  //     <Header onTab={onTab}></Header>
  //     {/* Slider */}
  //     <Main Tab={Tab}></Main>
  //     <Footer></Footer>
  //     <ThanhMeunuMobile onTab={onTab}></ThanhMeunuMobile>
  //   </React.Fragment>
  // );
}

export default App;
