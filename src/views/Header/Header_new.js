import React from 'react'

function Header_new() {
    return (
        <React.Fragment>
            <div id="wrapper-start">
  <div className="bg-open-canvas-menu" />
  <div className="mys-canvas-menu">
    <div className="close"><i className="bx bx-x" /></div>
    <div className="content-form-btn">
      <div className="login">
        <a href="https://muahang.nhaphangchina.vn/login" rel="nofollow" target="_blank">
          Đăng Nhập          </a>
      </div>
      <div className="resgister">
        <a href="https://muahang.nhaphangchina.vn/register?utm_source=direct&utm_medium=trang-chu&utm_campaign=trang-chu&utm_term=trang-chu&utm_content=trang-chu" rel="nofollow" target="_blank">
          Đăng ký          </a>
      </div>
      <div className="cart">
        <a href="https://muahang.nhaphangchina.vn/cart" rel="nofollow" target="_blank">
          Giỏ Hàng          </a>
      </div>
    </div>
    <div className="content-cv">
      <ul id="main-menu" className="canvas-main-menu">
        <li id="menu-item-93" className="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-93"><a href="index.html" aria-current="page">Trang Chủ</a></li>
        <li id="menu-item-554" className="menu-item menu-item-type-custom menu-item-object-custom menu-item-554"><a href="gioi-thieu-ve-nhap-hang-china/index.html">Giới thiệu</a></li>
        <li id="menu-item-94" className="menu-item menu-item-type-custom menu-item-object-custom menu-item-94"><a href="danh-muc-dich-vu.html">Dịch Vụ</a></li>
        <li id="menu-item-382" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-382"><a href="bang-gia-van-chuyen/index.html">Bảng Giá</a></li>
        <li id="menu-item-99" className="menu-item menu-item-type-custom menu-item-object-custom menu-item-99"><a href="huong-dan-dat-hang-tren-website-nhap-hang-china/index.html">Hướng Dẫn</a></li>
        <li id="menu-item-381" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-381"><a href="danh-muc-kinh-nghiem.html">Kinh nghiệm</a></li>
        <li id="menu-item-98" className="menu-item menu-item-type-custom menu-item-object-custom menu-item-98"><a href="danh-muc-chinh-sach.html">Chính Sách</a></li>
      </ul>
    </div>
    <div className="info">
      <div className="exchange-rate">
        <p>Tỷ giá: <span>3.750</span> </p>
      </div>
      <div className="hotline">
        <p>Hotline: <span>096.247.1688</span></p>
      </div>
    </div>
  </div>
</div>

        </React.Fragment>
    )
}

export default Header_new
