import React from 'react'
import hosting from '../../../host/hosting'

function LichSuDatHang() {
    const [LichSuDatHang,SetLichSuDatHang] = React.useState([])
    const getData = async ()=>{
        const DLDangNhapLucLac = window.localStorage.getItem('DLDangNhapLucLac') === null ? [] :
        JSON.parse(window.localStorage.getItem('DLDangNhapLucLac'))

        console.log(DLDangNhapLucLac)
        const response = await fetch(hosting.LichSuHoaDonTaiKhoanKhachHang+`/${DLDangNhapLucLac.map(x=>x.doi_tuong_id).toString()}`)
        const JsonData = await response.json()
        SetLichSuDatHang(JsonData)
    }
    React.useEffect(()=>{
        getData()
    },[])
    return (
<React.Fragment>
{/* Shopping Cart Section Start */}
<div className="section section-margin">
  <div className="container">
    <div className="row">
      <div className="col-12">
        {/* Cart Table Start */}
        <div className="cart-table table-responsive" style={{width:'100%'}}>
          <table className="table table-bordered">
            {/* Table Head Start */}
            <thead>
              <tr>
                <th className="pro-thumbnail">Mã hoá đơn</th>
                <th className="pro-quantity">Trạng thái hàng</th>
                <th className="pro-subtotal">Tổng tiền</th>
                
              </tr>
            </thead>
            {/* Table Head End */}
            {/* Table Body Start */}
            <tbody>
              {LichSuDatHang.map(x=>(
                      <tr>
                      <td className="pro-thumbnail">{x.hoa_don_ban_id}</td>
                      <td className="pro-quantity"
                      style={{textAlign:'left'}}>
                          {x.dl_trangthaihoadon.split('*').map(y=>(
                            <p style={{marginLeft:'20px'}}>{y}</p>
                          ))}
                      </td>
                      <td className="pro-subtotal"><span>{parseInt(x.tong_gia_tri).toLocaleString('vi', {style : 'currency', currency : 'VND'})}</span></td>
                    </tr>
              )
              )}
            
            </tbody>
            {/* Table Body End */}
          </table>
        </div>
        {/* Cart Table End */}
        {/* Cart Button Start */}
        {/* Cart Button End */}
      </div>
    </div>
  </div>
</div>
{/* Shopping Cart Section End */}

        </React.Fragment>
    )
}

export default LichSuDatHang
