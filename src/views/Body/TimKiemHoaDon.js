import React from 'react'
import hosting from '../../host/hosting'
function TimKiemHoaDon() {
    const [LichSuDatHang,SetLichSuDatHang] = React.useState([])
    const [TheoHoTen,SetTheoHoTen] = React.useState(' ')
    const [TheoSoDT,SetTheoSoDT] = React.useState(' ')
    const getData = async ()=>{
        const DLDangNhapLucLac = window.localStorage.getItem('DLDangNhapLucLac') === null ? [] :
        JSON.parse(window.localStorage.getItem('DLDangNhapLucLac'))

        console.log(DLDangNhapLucLac)
        const response = await fetch(hosting.LichSuHoaDonTaiKhoanKhachHang+`/${DLDangNhapLucLac.map(x=>x.doi_tuong_id).toString()}`)
        const JsonData = await response.json()
        SetLichSuDatHang(JsonData)
    }
    const onClickTimKiem = async ()=>{
        const response = await fetch(hosting.TimKiemHoaDon,{
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({TheoHoTen,TheoSoDT}),
        })
        const JsonData = await response.json()
        console.log(JsonData)
        SetLichSuDatHang(JsonData)
    }
    React.useEffect(()=>{
        getData()
    },[])
    return (
        <React.Fragment>
                        {/* Breadcrumb Section Start */}
<div className="section mb-5">
  {/* Breadcrumb Area Start */}
  <div className="breadcrumb-area bg-primary">
    <div className="container">
      <div className="breadcrumb-content">
        <ul>
          <li>
            <a href="./"><i className="fa fa-home" /> </a>
          </li>
          <li className="active"> Tìm kiếm hoá đơn</li>
        </ul>
      </div>
    </div>
  </div>
  {/* Breadcrumb Area End */}
</div>
{/* Breadcrumb Section End */}
{/* Shopping Cart Section Start */}
<div className="container">
    <div className="row">
        <div className="col" >
            Tìm kiếm theo họ và tên
            <input className="form-control" style={{fontSize:'14px'}} 
            onChange={async (e) =>SetTheoHoTen(e.target.value)}
            value={TheoHoTen}
            >

            </input>
        </div>
        <div className="col">
            Tìm kiếm theo số điện thoại
            <input className="form-control" style={{fontSize:'14px'}}
            onChange={async (e) =>SetTheoSoDT(e.target.value)}
            value={TheoSoDT}
            >
                
            </input>
        </div>
    </div>
    <div className="row mt-3">
        <div className="col">

        </div>
        <div className="col">
            <button className="btn btn-primary" onClick={onClickTimKiem}>Tìm kiếm</button>
        </div>
    </div>
</div>
<div className="section section-margin">
  <div className="container">
    <div className="row">
      <div className="col-12">
        {/* Cart Table Start */}
        <div className="cart-table table-responsive" style={{width:'100%'}}>
          <table className="table table-bordered">
            {/* Table Head Start */}
            <thead>
              <tr>
                <th className="pro-thumbnail">Mã hoá đơn</th>
                <th className="pro-quantity">Trạng thái hàng</th>
                <th className="pro-subtotal">Tổng tiền</th>
                
              </tr>
            </thead>
            {/* Table Head End */}
            {/* Table Body Start */}
            <tbody>
              {LichSuDatHang.map(x=>(
                      <tr>
                      <td className="pro-thumbnail">{x.hoa_don_ban_id}</td>
                      <td className="pro-quantity"
                      style={{textAlign:'left'}}>
                          {x.dl_trangthaihoadon.split('*').map(y=>(
                            <p style={{marginLeft:'20px'}}>{y}</p>
                          ))}
                      </td>
                      <td className="pro-subtotal"><span>{parseInt(x.tong_gia_tri).toLocaleString('vi', {style : 'currency', currency : 'VND'})}</span></td>
                    </tr>
              )
              )}
            
            </tbody>
            {/* Table Body End */}
          </table>
        </div>
        {/* Cart Table End */}
        {/* Cart Button Start */}
        {/* Cart Button End */}
      </div>
    </div>
  </div>
</div>
{/* Shopping Cart Section End */}

        </React.Fragment>
    )
}

export default TimKiemHoaDon
